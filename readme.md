# etcd build

This is a setup suitable for creating a docker image for running etcd from source. We use it for creating our custom docker image for ARMv7 platform for running etcd of Raspberry Pi devices. But the build neither depends on Raspberry Pi nor on ARM as a platform.

The setup is based on https://github.com/ender74/docker-etcd-arm. In opposition to that, this setup does not rely on another custom-built image and it does not include a whole bunch of implicit invocation arguments. In addition, it is using multiple stages to decrease the size of resulting image.

## Prerequisites

You need to install docker and docker-compose.

## Usage

* Open terminal in locally checked out project folder.

* Check the version on etcd declared in **Dockerfile**. Update it according to your needs.

  In addition, customize resulting image's name in **docker-compose.yml**. By default, resulting image is named `cepharum/etcd:arm`.

* Build the image by running

  ```bash
  docker-compose build etcd
  ```
