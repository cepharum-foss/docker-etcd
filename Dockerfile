FROM debian as build

ENV GOPATH=/opt/go
ENV GOOS=linux
ENV GOARCH=arm
ENV GOARM=7

ENV ETCD_VER=v3.5.4

WORKDIR /opt/go/src/github.com/coreos

RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install --no-install-recommends -y curl ca-certificates git golang -y \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* /root/.cache

RUN git clone --branch $ETCD_VER https://github.com/coreos/etcd.git \
    && cd etcd \
    && ./build


FROM debian

WORKDIR /opt/go/src/github.com/coreos

COPY --from=build /opt/go/src/github.com/coreos/etcd/bin/etcd /opt/go/src/github.com/coreos/etcd/bin/etcdctl /usr/local/bin/

ENV ETCD_UNSUPPORTED_ARCH=arm

EXPOSE 2379
EXPOSE 2380
EXPOSE 4001

CMD ["/usr/local/bin/etcd"]
